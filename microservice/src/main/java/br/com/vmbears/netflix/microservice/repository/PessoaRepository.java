package br.com.vmbears.netflix.microservice.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.vmbears.netflix.microservice.entity.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

	Pessoa findByNome(String nome);
	
	List<Pessoa> findByNomeContaining(String nome);

	Pessoa findByNomeAndSobrenome(String nome, String sobrenome);

	@Query("SELECT p FROM Pessoa p WHERE 0 <> (p.id % 2) ")
	Page<Pessoa> findIdImpar(Pageable pageable);
}
