package br.com.vmbears.netflix.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class Configuracao extends WebMvcConfigurationSupport {
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) { 
    	registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }

}