package br.com.vmbears.netflix.web.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Endereco {

	private Integer id;
	private String logradouro;
	private Integer numero;
	private String complemento;
	private Integer cep;

}
